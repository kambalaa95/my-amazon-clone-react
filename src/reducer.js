import { AirplayTwoTone } from "@material-ui/icons";

export const initialState = {
  basket: [],
  user: null,
};

// Selector
export const getBasketTotal = (basket) =>
  basket?.reduce((initialAmount, item) => item.price + initialAmount, 0);

// current state of application
// action - addToBasket? removeFromBasket?
const reducer = (state, action) => {
  console.log(action);
  switch (action.type) {
    case "ADD_TO_BASKET":
      return {
        ...state, // everything currently inside the state
        basket: [...state.basket, action.item], // original state + item added
      };

    // -----------

    case "REMOVE_FROM_BASKET":
      // find the first index where item in cart matches the actioned item
      const index = state.basket.findIndex(
        (cartItem) => cartItem.id === action.id
      );
      // grab the basket with its current state
      let newBasket = [...state.basket];
      // if there is such index
      if (index >= 0) {
        newBasket.splice(index, 1); // remove this specific index from the basket
      } else {
        console.warn(
          `The product (id: ${action.id}) is not in the basket. You can only remove products that are in the basket.`
        );
      }
      return {
        ...state,
        basket: newBasket,
      };
    // ---------
    case "SET_USER":
      return {
        ...state, // everything currently inside the state
        user: action.user,
      };
    // --------
    case "EMPTY_BASKET":
      return {
        ...state, // everything currently inside the state
        basket: [],
      };
    // --------
    default:
      return state;
  }
};

export default reducer;
