import React, { Component } from "react";
import "./Carousel.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

const photos = [
  {
    name: "Photo 1",
    url:
      "https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg",
  },
  {
    name: "Photo 2",
    url:
      "https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_Home_v2_en_US_1x._CB429090084_.jpg",
  },
  {
    name: "Photo 3",
    url:
      "https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_45M_v2_1x._CB432458380_.jpg",
  },
];

class Carousel extends Component {
  render() {
    const settings = {
      dots: false,
      fade: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      arrows: true,
      slidesToScroll: 1,
      className: "slides",
      autoplay: true,
      fade: false,
    };

    return (
      <div className="carousel">
        <Slider {...settings}>
          {photos.map((photo) => {
            return (
              <div className="slick-slide">
                <img src={photo.url} className="carousel__image" alt="" />
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}
export default Carousel;
