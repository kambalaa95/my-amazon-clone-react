import React, {useEffect, useState} from 'react';
import { db } from './firebase';
import "./Orders.css";
import { useStateValue } from "./StateProvider";
import Order from "./Order";
import CurrencyFormat from "react-currency-format";

function Orders() {
    const [{ basket, user }, dispatch] = useStateValue();
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        if(user){
            db
            .collection('users')
            .doc(user?.uid)
            .collection('orders')
            .orderBy('created', 'desc')
            .onSnapshot( snapshot => ( // gives realtime snapshot of database
              setOrders(snapshot.docs.map(doc => ({ // goes through orders in firebase and sets its details in list of objects
                  id: doc.id,
                  data: doc.data()
              })))
              ));
        } else{
            setOrders([]);
        }
        
    }, [user]);

    return (
        <div className="orders">
            <h1>Your Orders</h1>

            <div className="orders__order">
                {orders?.map(order => (
                    <Order order={order} />
                ))}

            </div>
        </div>
    )
}

export default Orders
