import React, { useRef } from 'react';
import "./Footer.css";
import { useWindowScroll } from "react-use";

const scrollToTop = () => window.scrollTo({top: 0, behavior: "smooth"})
function Footer() {
    return (
        <div className="footer">
            <div className="footer__backToTop" onClick={scrollToTop}>
                <span>Back to top</span>
            </div>
            <div className="footer__main">
                <div className="footer__row">
                    <div className="footer__getToKnowUs">
                        <h4>Get to Know Us</h4>
                        <p>Carrers</p>
                        <p>Blog</p>
                        <p>About Amazon</p>
                        <p>Investor Relations</p>
                        <p>Amazon Devices</p>
                        <p>Amazon Tours</p>
                    </div>
                    <div className="footer__makeMoneyWithUs">
                        <h4>Make Money with Us</h4>
                        <p>Sell products on Amazon</p>
                        <p>Sell apps on Amazon</p>
                        <p>Become an Affiliate</p>
                        <p>Advertise Your Products</p>
                        <p>Self-Publish with Us</p>
                        <p>Host an Amazon Hub</p>
                        <p>› See More Make Money with Us </p>
                    </div>
                    <div className="footer__AmazonPaymentProducts">
                        <h4>Amazon Payment Products</h4>
                        <p>Amazon Business Card</p>
                        <p>Shop with Points</p>
                        <p>Reload Your Balance</p>
                        <p>Amazon Currency Converter</p>
                    </div>
                    <div className="footer__LetUsHelpYou">
                        <h4>Let Us Help You</h4>
                        <p>Amazon and COVID-19</p>
                        <p>Your Account</p>
                        <p>Your Orders</p>
                        <p>Shipping Rates & <br/>Policies</p>
                        <p>Amazon Prime</p>
                        <p>Returns &<br/> Replacements</p>
                        <p>Manage Your Content<br/> and Devices</p>
                        <p>Amazon Assistant</p>
                        <p>Help</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
