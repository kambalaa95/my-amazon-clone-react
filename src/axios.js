import axios from "axios";

const instance = axios.create({
    baseURL: 'https://us-central1-my--clone-da8f5.cloudfunctions.net/api' // THE API (cloud function) URL
    // 'http://localhost:5001/my--clone-da8f5/us-central1/api' // just in case for debugging
});

export default instance;