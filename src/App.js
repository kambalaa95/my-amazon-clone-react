import React, { useEffect } from "react";
import "./App.css";
import Header from "./Header";
import Cart from "./Cart";
import Home from "./Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./Login";
import { auth } from "./firebase";
import { useStateValue } from "./StateProvider";
import Payment from "./Payment";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import Orders from "./Orders.js";
import HeaderTwo from "./HeaderTwo.js";
import Footer from "./Footer.js";


const promise = loadStripe(
  "pk_test_51HTWIiDnJ2eItEpr5PSZMphdXrSlmzs0YYN0XACSBekzvWVmIkuNiO4lN0tRghEXetGmI3O3OM1N3MjGn05blENc00QGqVwWlU"
);

function App() {
  const [{}, dispatch] = useStateValue();

  // its like reacts dynamic if statement
  // will only run once when App component runs
  useEffect(() => {
    // listener to keep track of who is signed in
    auth.onAuthStateChanged((authUser) => {
      console.log("THE USER IS >>>", authUser);
      if (authUser) {
        // the user is logged in
        dispatch({
          type: "SET_USER", // set user into the react context api (data layer)
          user: authUser,
        });
      } else {
        // the user is logged out
        dispatch({
          type: "SET_USER",
          user: null,
        });
      }
    });
  }, []); // if something in [] would change, it would then rerun the code, but since its empty it will only when App component runs.

  return (
    <Router>
      <div className="app">
        <Switch>
          <Route exact path="/">
            <Header />
            <HeaderTwo />
            <Home />
            <Footer />
          </Route>
          <Route path="/cart">
            <Header />
            <HeaderTwo />
            <Cart />
            <Footer />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/payment">
            <Header />
            <HeaderTwo />

            <Elements stripe={promise}>
              <Payment />
            </Elements>
            <Footer />
          </Route>
          <Route path="/orders">
            <Header />
            <HeaderTwo />
            <Orders />
            <Footer />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
