import React from "react";
import "./Home.css";
import Product from "./Product";
import Carousel from "./Carousel";

function Home() {
  return (
    <div className="home">
      {/* <img
        className="home__image"
        src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
        alt=""
      /> */}
      <Carousel />
      <div className="home__row" style={{ marginTop: "-150px" }}>
        <Product
          id="214314"
          title='"Lenovo IdeaCentre AIO 3, 24" All-in-One Computer, AMD Ryzen 3 4300U Mobile Processor, Integrated Graphics, 8GB DDR4, 256GB M.2 NVMe SSD, DVD RW Drive'
          price={549.95}
          image="https://images-na.ssl-images-amazon.com/images/I/81LvwMJOShL._AC_SY450_.jpg"
          rating={4}
        />{" "}
        <Product
          id="49538094"
          title="New Apple MacBook Pro (16-inch, 16GB RAM, 512GB Storage, 2.6GHz Intel Core i7) - Space Gray"
          price={2149.99}
          rating={5}
          image="https://images-na.ssl-images-amazon.com/images/I/81aot0jAfFL._AC_SL1500_.jpg"
        />
      </div>{" "}
      <div className="home__row">
        <Product
          id="4903850"
          title="Samsung Galaxy S10 Lite New Unlocked Android Cell Phone | 128GB of Storage"
          price={499.99}
          rating={5}
          image="https://images-na.ssl-images-amazon.com/images/I/61ChDFI70ML._AC_SL1500_.jpg"
        />
        <Product
          id="23445930"
          title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
          price={87.99}
          rating={5}
          image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"
        />
        <Product
          id="3254354345"
          title="New Apple iPad Pro (12.9-inch, Wi-Fi, 128GB) - Silver (4th Generation)"
          price={598.99}
          rating={4}
          image="https://images-na.ssl-images-amazon.com/images/I/816ctt5WV5L._AC_SX385_.jpg"
        />
      </div>{" "}
      <div className="home__row">
        <Product
          id="90829332"
          title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor - Super Ultra Wide Dual WQHD 5120 x 1440"
          price={1094.98}
          rating={4}
          image="https://images-na.ssl-images-amazon.com/images/I/6125mFrzr6L._AC_SX355_.jpg"
        />
      </div>{" "}

    </div>
  );
}

export default Home;
