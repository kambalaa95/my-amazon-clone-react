import React from "react";
import "./CartItem.css";
import { useStateValue } from "./StateProvider";

function CartItem({ id, title, price, rating, image, hideButton }) {
  const [{ basket }, dispatch] = useStateValue();

  const removeItem = () => {
    //remove item from the basket
    dispatch({
      type: "REMOVE_FROM_BASKET",
      id: id,
    });
  };
  return (
    <div className="cartItem">
      <img src={image} alt="" className="cartItem__image" />
      <div className="cartItem__info">
        <p className="cartItem__title">{title}</p>
        <p className="cartItem__price">
          <small>€</small>
          <strong>{price}</strong>
        </p>
        <div className="cartItem__rating">
          {Array(rating)
            .fill()
            .map((_, i) => (
              <p>⭐</p>
            ))}
        </div>
        {!hideButton && (  // only render if this prop is not specified when calling object
          <button onClick={removeItem}>Remove item</button>
        )}
    
      </div>
    </div>
  );
}

export default CartItem;
