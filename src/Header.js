import React from "react";
import "./Header.css";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { Link } from "react-router-dom";
import { useStateValue } from "./StateProvider";
import { auth } from "./firebase";
import HeaderTwo from "./HeaderTwo.js";

function Header() {
  // pulling basket and user FROM the STATE
  const [{ basket, user }, dispatch] = useStateValue();
  const signOutIfAuth = () => {
    if (user) {
      auth.signOut();
    }
  };

  return (
    
    <div className="header">
      
      <Link to="/">
        <img
          className="header__logo"
          src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"
        />
      </Link>
      <div className="header__search" style={{marginRight:"4px"}}>
        <input className="header__searchInput" type="text" />
        <SearchIcon className="header__searchIcon" />
      </div>
      <div className="header__nav">
        <Link to={!user && "/login"} style={{ textDecoration: 'none' }} className="hoverLink">
          <div onClick={signOutIfAuth} className="header__option">
            <span className="header__optionLineOne">
              {" "}
              Hey, {user ? user.email : "Guest"}{" "}
            </span>
            <span className="header__optionLineTwo">
              {/* if authenticated show sign out*/}
              {user ? "Sign Out" : "Sign In"}
            </span>
          </div>
        </Link>
        <Link to="/orders" style={{ textDecoration: 'none' }} className="hoverLink">
          <div className="header__option">
            <span className="header__optionLineOne"> Returns </span>
            <span className="header__optionLineTwo"> & Orders </span>
          </div>
        </Link>
        <div className="header__option hoverLink" style={{paddingRight:"10px", paddingLeft:"10px"}}>
          <span className="header__optionLineOne"> Your </span>
          <span className="header__optionLineTwo"> Prime </span>
        </div>
        <Link to="/cart" className="hoverLink" style={{ textDecoration: 'none' }}>
          <div className="header__optionBasket">
            <ShoppingCartIcon />
            <span className="header__optionLineTwo header__basketCount">
              {basket?.length}
              {/* ? -- optional chaining -- basket is undefined/dont have correct value -> won't break the app  */}
            </span>
          </div>
        </Link>
      </div>
    </div>
  );
}

export default Header;
