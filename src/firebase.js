import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyANp57KRh46lvrSBvUdaBcxMmsu3eSpHWQ",
  authDomain: "my--clone-da8f5.firebaseapp.com",
  databaseURL: "https://my--clone-da8f5.firebaseio.com",
  projectId: "my--clone-da8f5",
  storageBucket: "my--clone-da8f5.appspot.com",
  messagingSenderId: "378439350084",
  appId: "1:378439350084:web:53484770b9c00387b08911",
  measurementId: "G-3ZS9PDP97Q",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

// initialize database
const db = firebaseApp.firestore();
// initialize auth - will provide auth functionality
const auth = firebase.auth();

export { db, auth };
