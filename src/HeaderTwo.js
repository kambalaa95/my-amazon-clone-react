import React from 'react'
import "./HeaderTwo.css";
import LocationOnIcon from '@material-ui/icons/LocationOn';

function HeaderTwo() {
    return (
        <div className="headerTwo">
            <div className="headerTwo__deliverTo ">
                <div className="hoverLink" style={{display: "flex", width:"90px"}}>
                <div className="headerTwo__deliverToIcon">
                    <LocationOnIcon />
                </div>
                <div style={{display:"flex", flexDirection: "column"}}>
                    <span><small>Deliver to</small></span>
                    <span className="headerTwo__optionLineTwo">Ireland</span>
                </div>
                </div>
            </div>
            
            <div className="headerTwo__midLinksContainer">
                {/* <p className="headerTwo__midLink">Today's Deals</p>
                <p className="headerTwo__midLink">Customer Service</p>
                <p className="headerTwo__midLink">Gift Cards</p>
                <p className="headerTwo__midLink">Registry</p>
                <p className="headerTwo__midLink">Sell</p> */}
                <span className="hoverLink">Today's Deals</span>
                <span className="hoverLink">Customer Service</span>
                <span className="hoverLink">Gift Cards</span>
                <span className="hoverLink">Registry</span>
                <span className="hoverLink">Sell</span>
            </div>
            <div className="headerTwo__responseToCovid">
                <p className="hoverLink">Amazon's response to COVID-19</p>
            </div>
            
        </div>
    )
}

export default HeaderTwo
