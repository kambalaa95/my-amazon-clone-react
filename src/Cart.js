import React from "react";
import "./Cart.css";
import { useStateValue } from "./StateProvider";
import Subtotal from "./Subtotal";
import CartItem from "./CartItem";
import { ShoppingBasket } from "@material-ui/icons";

function Cart() {
  const [{ basket }, dispatch] = useStateValue();

  return (
    <div className="cart">
      <div className="cart__left">
        <img
          // src="https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_.jpg"
          src="https://cdn.pixabay.com/photo/2017/10/27/15/12/geeks-2894621_1280.jpg"
          // width="370px"
          height="70px"
          alt=""
          className="cart__ad"
        />

        <div>
          <h2 className="cart__title">Shopping Cart</h2>

          {basket.map((item) => (
            <CartItem
              id={item.id}
              title={item.title}
              price={item.price}
              rating={item.rating}
              image={item.image}
            />
          ))}
        </div>
      </div>
      <div className="cart___right">
        <Subtotal />
      </div>
    </div>
  );
}

export default Cart;
