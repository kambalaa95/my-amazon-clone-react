const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const stripe = require('stripe')('sk_test_51HTWIiDnJ2eItEprCE2eDVMPMiKHcQVZSUUUi9J5StJQSQ6Rh2pLYc2VjZbqwA8orE85o6uAZZsarclzjDhKMBpx00M3yHIdWG');

// --- API ---
// 1:16:31
// --- App config --- 
const app = express();

// --- Middleware ---
app.use(cors({
    origin: true
}));
app.use(express.json()); // send/pass data in json format

// --- API routes ---
app.get('/', (request, response) => response.status(200).send('testing API ;)'));

app.post('/payments/create', async (request, response) => {
    const total = request.query.total; // access the total param posted in stripe secret method (getClientsSecret() --> Payments.js)
    console.log('Payment request received. Total value: ', total);

    const paymentIntent = await stripe.paymentIntents.create({
        amount: total, // subunits of the currency
        currency: 'eur',
    });

    // payment intent created!
    response.status(201).send({
        clientSecret: paymentIntent.client_secret,
    });

});

// --- Listen command ---
exports.api = functions.https.onRequest(app);


// API END POINT retrieved after running emulator
// http://localhost:5001/my--clone-da8f5/us-central1/api





// firebase emulators:start







// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });